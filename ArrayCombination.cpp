/*
Made by Kevin Clark on 10/22/2017.  This one's going on Github for sure.

STEPS:
Declare 3 arrays, 2 of size 20, and 1 of size 40 -- main()
Read in data from 2 files into the size 20 arrays -- ReadInArray()
Check if size 20 arrays are sorted.  If yes, continue, if not, exit with -1 and print -- CheckIfSorted()
Combine the sorted arrays in order of lowest to highest -- Combine()
Check which array ran out -- Combine()
Once one array has ran out, put all other values from the other array into size 40 array -- Combine()
Print the values inside the 40 array into a file -- PrintToFile() 
*/
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int ReadInArray(int SourceArray[], ifstream& infile)
{
	const int ArraySize = 20;
	int ArrayIndexCounter = 0;
	
	//Read in numbers from a file into the input array.  Stop if there are more than 20 numbers, or if the file runs out of data.
	while((ArrayIndexCounter < ArraySize) && (infile >> SourceArray[ArrayIndexCounter]))
	{
		ArrayIndexCounter++;
	}
	return ArrayIndexCounter; //Return the total number of items the input array.
}

bool CheckIfSorted(int SourceArray[], int ArraySize)
{
	for(int i = 1; i < ArraySize; i++)
	{
		if (SourceArray[i] < SourceArray[i-1])
		{
			return false; //The array is not in a sorted order.
		}
	}
	
	return true; //The array is in sorted order.
}

int Combine(const int SourceArray1[], const int SourceArray2[], int DestArray[], int Array1Size, int Array2Size)
{
	int Array1Counter = 0, Array2Counter = 0, DestArrayCounter = 0;
	
	while((Array1Counter < Array1Size) && (Array2Counter < Array2Size)) //While neither array is exausted of numbers...
	{
		if(SourceArray1[Array1Counter] < SourceArray2[Array2Counter]) //If the number in array1 is less than array2,
		{
			DestArray[DestArrayCounter] = SourceArray1[Array1Counter]; //Put the number from array1 into the destination array.
			Array1Counter++;
		}
		else
		{
			DestArray[DestArrayCounter] = SourceArray2[Array2Counter]; //Otherwise put the number from array2 into the destination array.
			Array2Counter++;
		}
		DestArrayCounter++;
	}
	
	if(Array1Counter == Array1Size) //If array1 is exausted of numbers,
	{
		while(Array2Counter < Array2Size) //Put the rest of the numbers from Array2 into the destination array.
		{
			DestArray[DestArrayCounter] = SourceArray2[Array2Counter];
			DestArrayCounter++;
			Array2Counter++;
		}
	}
	else //Array2 must be exausted of numbers
	{
		while(Array1Counter < Array1Size) //Put the rest of the numbers from Array1 into the destination array.
		{
			DestArray[DestArrayCounter] = SourceArray1[Array1Counter];
			DestArrayCounter++;
			Array1Counter++;
		}
	}
	
	return DestArrayCounter; //Returns the number of items in the destination array.
}

void PrintToFile(const int DestArray[], int DestArraySize, ostream& outfile)
{
	for(int i = 0; i < DestArraySize; i++) //For how big the destination array is,
	{
		outfile << DestArray[i] << " "; //Print each value in destination array and a space after(" ").
	}
	outfile << endl;
}

int main()
{
	//Variable and object declarations, reading filenames, and opening files.
	int SourceArray1[20], SourceArray2[20],DestArray[40];
	int Array1Size, Array2Size, DestArraySize;
	string OutFileName, InFileName1, InFileName2;
	bool IsSorted1, IsSorted2;
	ifstream infile1, infile2;
	ofstream outfile;
	cout << "First Input File Name?: ";
	cin >> InFileName1;
	infile1.open(InFileName1.c_str());
	cout << "Second Input File Name?: ";
	cin >> InFileName2;
	infile2.open(InFileName2.c_str());
	cout << "Output File Name?: ";
	cin >> OutFileName;
	outfile.open(OutFileName.c_str());
	
	Array1Size = ReadInArray(SourceArray1, infile1);
	Array2Size = ReadInArray(SourceArray2, infile2);
	
	IsSorted1 = CheckIfSorted(SourceArray1, Array1Size);
	IsSorted2 = CheckIfSorted(SourceArray2, Array2Size);
	
	if(!(IsSorted1 && IsSorted2)) //If either array is not sorted,
	{
		cout << "One or more source files contain nonsorted numbers.  Exiting." << endl;
		return -1; //Display a message and terminate the program.
	}
	
	DestArraySize = Combine(SourceArray1, SourceArray2, DestArray, Array1Size, Array2Size);
	PrintToFile(DestArray, DestArraySize, outfile);
	
	return 0;
}